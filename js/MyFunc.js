/**
 * Created by wenya on 2017/7/25.
 */
/**
 * 滚动距离
 * @returns {*}
 */
function scroll() {
    if(window.pageYOffset || window.pageXOffset){
        // 最新浏览器
        return {
            top:window.pageYOffset,
            left:window.pageXOffset
        }
    }else if(document.compatMode == 'CSS1Compat'){
        // 标准浏览器
        return {
            top:document.documentElement.scrollTop,
            left:document.documentElement.scrollLeft
        }
    }else {
        // 怪异模式
        return {
            top:document.body.scrollTop,
            left:document.body.scrollLeft
        }
    }
}